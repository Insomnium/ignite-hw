package net.ins.ignite.conf;

import net.ins.ignite.domain.Statement;
import net.ins.ignite.domain.User;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author Ins137@gmail.com
 *         Created at 8/18/2017
 */
@Configuration
@EnableConfigurationProperties(SampleProperties.class)
public class TestConfig {

    private static final String TEST_CACHE_SUFFIX = "_test";

    @Autowired
    private SampleProperties properties;


    @Bean
    @Primary
    public Ignite ignite() {
        return Ignition.start(new IgniteConfiguration());
    }

    @Bean
    @Primary
    public IgniteCache<String, Statement> statementCache(Ignite ignite) {
        return ignite.getOrCreateCache(properties.getStatement().getName() + TEST_CACHE_SUFFIX);
    }

    @Bean
    @Primary
    public IgniteCache<String, User> userCache(Ignite ignite) {
        return ignite.getOrCreateCache(properties.getUsers().getName() + TEST_CACHE_SUFFIX);
    }
}
