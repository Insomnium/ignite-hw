package net.ins.ignite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IgnitePocApplication {

	public static void main(String[] args) {
		SpringApplication.run(IgnitePocApplication.class, args);
	}
}
