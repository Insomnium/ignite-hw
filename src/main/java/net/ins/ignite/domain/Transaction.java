package net.ins.ignite.domain;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * @author Ins137@gmail.com
 *         Created at 8/18/2017
 */
@Data
public class Transaction implements Serializable {
    private String srcAccount;
    private String dstAccount;
    private BigDecimal amount;
    private ZonedDateTime dateTime;
}
