package net.ins.ignite.domain;

import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.ignite.cache.affinity.AffinityKeyMapped;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.time.ZonedDateTime.now;

/**
 * @author Ins137@gmail.com
 *         Created at 8/18/2017
 */
@Data
@Accessors(chain = true)
public class Statement implements Serializable {
    private Integer version = 0;
    @AffinityKeyMapped
    private String userId;
    private List<Transaction> transactions = new ArrayList<>();
    private ZonedDateTime timestamp = now();
}
