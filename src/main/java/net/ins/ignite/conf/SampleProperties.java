package net.ins.ignite.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("ignite.cache")
@Data
public class SampleProperties {
    private Cache statement;
    private Cache users;

    @Data
    public static class Cache {
        private String name;
    }
}
