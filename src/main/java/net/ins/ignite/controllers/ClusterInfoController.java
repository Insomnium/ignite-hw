package net.ins.ignite.controllers;

import lombok.RequiredArgsConstructor;
import net.ins.ignite.domain.Statement;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCluster;
import org.apache.ignite.cache.CachePeekMode;
import org.apache.ignite.cluster.ClusterNode;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * @author Ins137@gmail.com
 *         Created at 8/21/2017
 */
@RestController
@RequestMapping(ClusterInfoController.INFO)
@RequiredArgsConstructor
class ClusterInfoController {

    public static final String INFO = "/cluster";

    private final Ignite ignite;

    private final IgniteCache<String, Statement> statementCache;

    @GetMapping("/nodes")
    public Collection<ClusterNode> listNodes() {
        IgniteCluster cluster = ignite.cluster();
        return cluster.nodes();
    }

    @GetMapping("/statementCacheSize")
    public int showOffHeapCacheEntries(@RequestParam(value = "peek", defaultValue = "OFFHEAP") CachePeekMode cachePeekMode) {
        throw new UnsupportedOperationException(); // TODO: return number of offheap cache entries.
    }
}
