package net.ins.ignite.controllers.handlers;

import lombok.extern.slf4j.Slf4j;
import net.ins.ignite.exceptions.NoStatementFoundException;
import net.ins.ignite.exceptions.NoUserFoundException;
import net.ins.ignite.exceptions.UserAlreadyExistsException;
import org.apache.ignite.transactions.TransactionException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * @author Ins137@gmail.com
 *         Created at 8/18/2017
 */
@ControllerAdvice
@Slf4j
public class SampleIgniteControllerHandler {

    @ExceptionHandler(NoStatementFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public void handleNoStatementFound(NoStatementFoundException exception) {
        log.error("No statement found for id: {}", exception.getStatementId());
    }

    @ExceptionHandler(NoUserFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public void handleNoUserFound(NoUserFoundException exception) {
        log.error("No user found for id: {}", exception.getUserId());
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    @ResponseStatus(CONFLICT)
    public void handleNoUserFound(UserAlreadyExistsException exception) {
        log.error("User with id {} already exists", exception.getUserId());
    }

    @ExceptionHandler(TransactionException.class)
    @ResponseStatus(CONFLICT)
    public void handleTransactionException(TransactionException transactionException) {
        log.error("Concurrent transaction exception", transactionException);
    }
}
