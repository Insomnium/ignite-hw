# Homework

## I. Caches; Concurrent transactions 
1. [Download stable Apache Ignite version (2.6.0)](https://ignite.apache.org/download.cgi) 
2. Run cluster of 2 server nodes in default configuration (multicast cluster members discovery)
3. Change this application's configuration to connect Ignite cluster
4. Make sure that application is in client mode
5. Add transaction concurrency mode and isolation level in `SampleIgniteController#updateStatement` controller method:

  * first, add some code to make concurrent transaction fail if statements has already been updated in by another client node
  * second, change code in order to make transaction wait for other concurrent transactions on another client node to be committed or rolled back  
  
*Hint - run two client application instances in parallel and set up breakpoints. Use -Dserver.port jvm arg to run processes on different ports*
6. Use `ignitevisor` to

  * check current cluster topology
  * check caches stats

## II. Ignite visor
1. Run `ignitevisor` and connect to cluster
2. Ask for current topology info
3. Search for memory usage stats
4. Find out number of caches entries
5. Check OFFHEAP and ONHEAP cache entries count via `ClusterInfoController#showOffHeapCacheEntries`:

## III. Setup Ignite Persistence

### WARNING! Cluster needs 80% of memory available on machine. Limit memory with defaultMemoryPolicySize to make run nodes successfully

1. Change default ignite configuration file to enable native persistence.
2. Run application again to connect to the cluster. It should fail because of cluster inactivity. Change `IgniteConfig#ignite` bean method to fix this problem
3. Run cluster of 1 or more servers
4. Run application as a client node
5. Put some values into statement cache (via `localhost:8080/ignite/statement/00001`)
6. Terminate all cluster nodes
7. Terminate client application
8. Run one or more server nodes again (check there is a `work` folder occurred in ignite directory)
9. Run client again to re-activate cluster
10. Make sure that all the data is still in the cache `curl -XGET localhost:8080/ignite/statement/00001`

## III. SQL-Grid (DML/DDL)*
Try to reimplement caches configurations and insert/update controller methods using SQL: DDL and DML
 
# Postman 
Don't you use [Postman](https://www.getpostman.com/) yet? Here is a [collection link](https://www.getpostman.com/collections/862c4bfc812940990c68) that helps you to interact with application.


#Help

## Gradle build
Run `./gradlew clean build` to build project

## Run application from IDEA
Enable **"Enable annotation processing"** checkbox to run application from IntelliJ IDEA in settings below: 
`Build, Execution, Deployment > Compiler > Annotation processors` 